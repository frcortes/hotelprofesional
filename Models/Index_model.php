<?php
    class Index_model extends Connection {
        public function __construct() {
            parent::__construct();
        }

        public function obtenerHabitaciones() {
            $where = 'estado = 1';
            return $this->db->select('*', 'habitaciones', $where, null);
        }

        public function obtenerHabitacion($idRoom) {
            $where = 'h.tipo_habitacion = th.idtipo_habitacion and idhabitaciones = :idRoom';
            $param = array('idRoom' => $idRoom);
            return $this->db->select('h.numHabitacion, th.descripcion, th.precio', 'habitaciones h, tipo_habitacion th', $where, $param);
        }
    }